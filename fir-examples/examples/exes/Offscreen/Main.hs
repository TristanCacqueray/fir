module Main where

import FIR.Examples.Offscreen.Application
  ( offscreen )

main :: IO ()
main = offscreen
