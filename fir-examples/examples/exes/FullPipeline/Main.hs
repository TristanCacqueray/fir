module Main where

import FIR.Examples.FullPipeline.Application
  ( fullPipeline )

main :: IO ()
main = fullPipeline
