{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NamedWildCards #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -Wno-missing-local-signatures #-}

module FIR.Examples.JuliaSet.Shaders where

-- base
import Data.Foldable
  ( sequence_,
  )
import Data.Maybe
  ( fromJust,
  )
-- filepath

-- text-short
import Data.Text.Short
  ( ShortText,
  )
-- vector-sized
import qualified Data.Vector.Sized as Vector
  ( fromList,
  )
-- fir
import FIR
-- fir-examples
import FIR.Examples.Paths
  ( shaderDir,
  )
import FIR.Syntax.Labels
import GHC.TypeNats
  ( KnownNat,
  )
import Math.Linear
import System.FilePath
  ( (</>),
  )

------------------------------------------------
-- pipeline input

type VertexInput =
  '[Slot 0 0 ':-> V 3 Float]

-------------------------------------
-- vertex shader

type VertexDefs =
  '[ "in_position" ':-> Input '[Location 0] (V 3 Float),
     "main" ':-> EntryPoint '[] Vertex
   ]

vertex :: ShaderModule "main" VertexShader VertexDefs _
vertex = shader do
  ~(Vec3 x y z) <- get @"in_position"
  put @"gl_Position" (Vec4 x y z 1)

------------------------------------------------
-- fragment shader

type FragmentDefs =
  '[ "out_colour" ':-> Output '[Location 0] (V 4 Float),
     "ubo"
       ':-> Uniform
              '[Binding 0, DescriptorSet 0]
              (Struct '["mousePos" ':-> V 2 Float]),
     "main" ':-> EntryPoint '[OriginUpperLeft] Fragment
   ]

complexSquare :: Code (V 2 Float) -> Code (V 2 Float)
complexSquare (Vec2 x y) = Vec2 (x * x - y * y) (2 * x * y)

complexLog :: Code (V 2 Float) -> Code (V 2 Float)
-- complexLog (Vec2 x y) = Vec2 (x * x - y * y) (2 * x * y)
complexLog z =
  let zC = ComplexFloat z
   in codeComplex $ (log (magnitude z) :+: phase z)

gradient ::
  forall n.
  KnownNat n =>
  Code Float ->
  Code (Array n (V 4 Float)) ->
  Code (V 4 Float)
gradient t colors =
  ((1 - s) *^ (view @(AnIndex _) i colors))
    ^+^ (s *^ (view @(AnIndex _) (i + 1) colors))
  where
    n :: Code Float
    n = Lit . fromIntegral $ knownValue @n
    i :: Code Word32
    i = floor ((n -1) * t)
    s :: Code Float
    s = (n -1) * t - fromIntegral i

sunset :: Array 9 (V 4 Float)
sunset =
  MkArray . fromJust . Vector.fromList $
    [ V4 0 0 0 0,
      V4 0.28 0.1 0.38 1,
      V4 0.58 0.2 0.38 1,
      V4 0.83 0.3 0.22 1,
      V4 0.98 0.45 0.05 1,
      V4 0.99 0.62 0.2 1,
      V4 1 0.78 0.31 1,
      V4 1 0.91 0.6 1,
      V4 1 1 1 1
    ]

maxDepth :: Code Word32
maxDepth = 256

xSamples, ySamples :: Code Word32
xSamples = 4
ySamples = 4

xWidth, yWidth :: Code Float
xWidth = recip . fromIntegral $ xSamples
yWidth = recip . fromIntegral $ ySamples

fragment :: ShaderModule "main" FragmentShader FragmentDefs _
fragment = shader do
  ~(Vec4 x y _ _) <- #gl_FragCoord
  ~(Vec2 mx my) <- use @(Name "ubo" :.: Name "mousePos")

  let -- disambiguate to help type inference
      (#<) :: _ => Program i i (Code a) -> Program i i (Code a) -> Program i i (Code Bool)
      (#<) = (<)

  #total #= (0 :: Code Word32)

  #xSampleNo #= (0 :: Code Word32)
  while (#xSampleNo #< pure xSamples) do
    #ySampleNo #= (0 :: Code Word32)
    while (#ySampleNo #< pure ySamples) do
      xNo <- #xSampleNo
      yNo <- #ySampleNo

      let dx, dy :: Code Float
          dx = (fromIntegral xNo + 0.5) * xWidth - 0.5
          dy = (fromIntegral yNo + 0.5) * xWidth - 0.5

      #pos #= Vec2 ((x + dx -960) / 250) ((y + dy -540) / 250)
      #depth #= (0 :: Code Word32)

      let c = Vec2 ((mx -960) / 600) ((my -540) / 600)

      loop do
        pos <- #pos
        depth <- #depth
        if (pos ^.^ pos > 4 || depth > maxDepth)
          then break @1
          else do
            let Vec2 posR posI = pos
                newPos = Vec2 posR (abs posI)
                newPos' = newPos ^+^ c
                newPos'' = complexSquare newPos'
                newPos''' = complexLog newPos''
            #pos .= newPos'''
            #depth .= depth + 1

      depth <- #depth
      #total %= (+ depth)

      #ySampleNo %= (+ 1)
    #xSampleNo %= (+ 1)

  total <- #total
  t <-
    let' @(Code Float) $
      log (fromIntegral total * xWidth * yWidth) / log (fromIntegral maxDepth)

  let col = gradient t (Lit sunset)

  #out_colour .= col

------------------------------------------------
-- compiling

vertPath, fragPath :: FilePath
vertPath = shaderDir </> "juliaset_vert.spv"
fragPath = shaderDir </> "juliaset_frag.spv"

compileVertexShader :: IO (Either ShortText ModuleRequirements)
compileVertexShader = compileTo vertPath [] vertex

compileFragmentShader :: IO (Either ShortText ModuleRequirements)
compileFragmentShader = compileTo fragPath [] fragment

compileAllShaders :: IO ()
compileAllShaders =
  sequence_
    [ compileVertexShader,
      compileFragmentShader
    ]

shaderPipeline :: ShaderPipeline FilePath
shaderPipeline =
  ShaderPipeline $
    StructInput @VertexInput @(Triangle List)
      :>-> (vertex, vertPath)
      :>-> (fragment, fragPath)
